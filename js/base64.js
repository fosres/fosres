var btoa = require('btoa');

var atob = require('atob');

function base64_encode(buf)	{

	return btoa(buf);
}

function base64_decode(s)	{

	return atob(s);
}

var str = process.argv[2];

bufferText = Buffer.from(str,'base64');

encodedStr = base64_encode(bufferText);

process.stdout.write("raw input:0x");

console.log(bufferText.toString('hex'));

process.stdout.write("base64:");

console.log(encodedStr);
//5
