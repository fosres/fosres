package main

import	(

	"fmt"
//	"encoding/base64"		
	"os"

// 	Get rid of any unnecessary imported libraries.

//	Uncomment "encoding/base64" if you want to use it.
)

func base64_encode(in []byte,in_size uint64) (result string)		{
	
// 	Type your code here ...
	
	return result;
}

func base64_decode(in []byte,in_size uint64) (result []byte, err error)	{
	
// 	Type your code here ...
	
	return result, err;
}

func main()							{
	

	if ( len(os.Args) < 2 )	{
		
		fmt.Printf("raw input:0x\n");	
		
		fmt.Printf("base64:\n");

		return;
	}


	var input []byte;

	var err error;

	input, err = base64_decode([]byte(os.Args[1]),uint64(len(os.Args[1])));

	if err != nil	{
		
		fmt.Println(err);
	}

	var output string = "";

	output = base64_encode(input,uint64(len(input)));

	fmt.Printf("raw input:0x");

	var i uint64 = 0;

	for ; i < uint64(len(input));		{

		fmt.Printf("%.2x",input[i]);

		i++;
	}

	fmt.Printf("\n");

	fmt.Printf("base64:%s\n",output);

}
//1
