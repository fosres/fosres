package main

import (
	"fmt"
	"strconv"	
	"database/sql"
	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
)

func sqltest() {

	database, _ := sql.Open( "sqlite3", "./nrabody.db")

	statement, _ := database.Prepare( "CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")

	statement.Exec()

	statement, _ = database.Prepare("INSERT INTO people (firstname,lastname) VALUES (?,?)")

	statement.Exec("Nic","Raboy")	

	rows, _ := database.Query("SELECT id,firstname,lastname FROM people")
	
	var id int

	var firstname string

	var lastname string

	for rows.Next()	{

		rows.Scan(&id,&firstname,&lastname)
	
	}

	fmt.Println(strconv.Itoa(id) + ": " + firstname + " " + lastname)
}

func main()	{

	sqltest();
}
