package main

import	(
	"fmt"
	"log"
	"os"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

func createTable(db * sql.DB)	{

	createSQLTable := `CREATE TABLE base64_tests (
	"iduser" TEXT NOT NULL PRIMARY KEY,
	"input" TEXT,
	"submissions" INTEGER,
	"hits" INTEGER);` // SQL Statement for Create Table

	log.Println("Create base64_tests table...")

	statement, err := db.Prepare(createSQLTable)

	if err != nil	{
		log.Fatal(err.Error())
	}

	statement.Exec()	// Execute SQL Statements

	log.Println("base64_tests created")

}

func insertSubmission(db * sql.DB,iduser string,input string,submissions uint64,hits uint64)	{

	log.Println("Inserting test records...")

	insertSQLTable := `INSERT INTO base64_tests(iduser,input,submissions,hits) VALUES (?,?,?,?)`

	statement, err := db.Prepare(insertSQLTable)

	if err != nil	{
		
		log.Fatalln(err.Error())
	}

	_, err = statement.Exec(iduser,input,submissions,hits)

	if err != nil	{
		log.Fatalln(err.Error())
	}
}

func displayStudents(db *sql.DB)	{
	
	row, err := db.Query("SELECT * FROM base64_tests ORDER BY hits")

	if err != nil	{
		
		log.Fatal(err)
	}

	defer row.Close()

	for row.Next()	{

		var iduser string
		
		var input string

		var submissions uint64

		var hits	uint64

		row.Scan(&iduser,&input,&submissions,&hits)

		fmt.Printf("%s:\n%s\n%d\n%d\n",iduser,input,submissions,hits)

}

}
 
func main()	{
		
	file, err := os.Create("base64_database.db")

	if err != nil	{
	
		log.Fatal(err.Error())

	}

	file.Close()

	log.Println("base64_database.db created")

	sqliteDatabase, _ := sql.Open("sqlite3","./base64_database.db")

	defer sqliteDatabase.Close() 

	createTable(sqliteDatabase)

	// INSERT RECORDS

	insertSubmission(sqliteDatabase,"fosres","\n\nZg==\nZg==\nZm8=\nZm8=\nZm9v\nZm9v\nZm9vYg==\nZm9vYg==\nZm9vYmE=\nZm9vYmE=\n",0,0)

	displayStudents(sqliteDatabase)
}
