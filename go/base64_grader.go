package main

import	(

	"fmt"
	"encoding/base64"
	"os"

)

func main()							{
	
	if ( len(os.Args) < 2 )	{
		
		fmt.Printf("raw input:0x\n");	
		
		fmt.Printf("base64:\n");

		return;
	}


	var input []byte;

	var err error;

	input, err = base64.StdEncoding.DecodeString(os.Args[1]);

	if err != nil	{
		
		fmt.Println(err);
	}

	var output string = "";

	output = base64.StdEncoding.EncodeToString(input);

	fmt.Printf("raw input:0x");

	var i uint64 = 0;

	for ; i < uint64(len(input));	{

		fmt.Printf("%.2x",input[i]);

		i++;
	}

	fmt.Printf("\n");

	fmt.Printf("base64:%s\n",output);

}
//1
