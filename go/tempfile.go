package main

import	(
	"io/ioutil"
	"log"
	"os"
	"fmt"
)

func main()	{

	tmpFile, err := ioutil.TempFile(os.TempDir(),"prefix.txt")

	if err != nil	{
		
		log.Fatal("Failed to make temporary file",err)

	}

	defer os.Remove(tmpFile.Name())

	fmt.Println("Created File: "+  tmpFile.Name())
}
