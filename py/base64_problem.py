import base64
import sys
import codecs
import binascii

def base64_encode(buf):
	
	#Input:buf is a bytes array object
	
	#Assign and return string object s
	
	s = "" 
	
	return s

def base64_decode(s):
	
	#Input:s is a string object
	
	#Assign and return a bytes array object buf
	
	buf = b""	
	
	return buf


data = sys.argv[1]


msg = "raw input:0x"

sys.stdout.buffer.write(bytes(msg.encode('ascii')))	

decodedStr = base64_decode(data.encode('ascii'))

bytearr = binascii.hexlify(decodedStr)

sys.stdout.buffer.write(bytearr)

msg = "\n"

sys.stdout.buffer.write(bytes(msg.encode('ascii')))

msg = "base64:"

sys.stdout.buffer.write(bytes(msg.encode('ascii')))

encodedBytes = base64_encode(decodedStr)

sys.stdout.buffer.write(bytes(encodedBytes.encode('ascii')))

msg = "\n"

sys.stdout.buffer.write(bytes(msg.encode('ascii')))
#4
